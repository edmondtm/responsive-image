// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCneDjkboSKbhIm456sOBa6dcj3AYEyhqI',
    authDomain: 'sharp-byloka.firebaseapp.com',
    databaseURL: 'https://sharp-byloka.firebaseio.com',
    projectId: 'sharp-byloka',
    storageBucket: 'sharp-byloka.appspot.com',
    messagingSenderId: '285437583545',
    appId: '1:285437583545:web:44ee9baa55c12e5e'
  },
  reCaptcha: '6LeauaoUAAAAAHXBmoTZGgq-8YIByfLkgUPkzKMW',
  cloudFunctions: 'https://us-central1-sharp-byloka.cloudfunctions.net/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
