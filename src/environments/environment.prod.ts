export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyCneDjkboSKbhIm456sOBa6dcj3AYEyhqI',
    authDomain: 'sharp-byloka.firebaseapp.com',
    databaseURL: 'https://sharp-byloka.firebaseio.com',
    projectId: 'sharp-byloka',
    storageBucket: 'sharp-byloka.appspot.com',
    messagingSenderId: '285437583545',
    appId: '1:285437583545:web:44ee9baa55c12e5e'
  },
  reCaptcha: '6LeauaoUAAAAAHXBmoTZGgq-8YIByfLkgUPkzKMW',
  cloudFunctions: 'https://us-central1-sharp-byloka.cloudfunctions.net/'
};
