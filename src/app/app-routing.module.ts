import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [
  {
    path: 'images',
    loadChildren: () => import('./images/images.module').then(mod => mod.ImagesModule)
  },
  {
    path: 'sendgrid',
    loadChildren: () => import('./sendgrid/sendgrid.module').then(mod => mod.SendgridModule)
  },
  {
    path: 'captcha',
    loadChildren: () => import('./captcha/captcha.module').then(mod => mod.CaptchaModule)
  },
  {
    path: 'menu',
    component: MenuComponent
  },
  {
    path: '',
    redirectTo: '/menu',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
