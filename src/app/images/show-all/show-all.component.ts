import { Component, OnInit, OnDestroy } from '@angular/core';
import { ImageService } from '../image.service';
import { Observable } from 'rxjs';
import { Image } from 'src/app/model/image';
import { AngularFireStorage } from '@angular/fire/storage';
import { take, map, distinctUntilChanged, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show-all',
  templateUrl: './show-all.component.html',
  styleUrls: ['./show-all.component.css']
})
export class ShowAllComponent implements OnInit, OnDestroy {

  constructor(
    private imageService: ImageService,
    private router: Router,
    private storage: AngularFireStorage) { }
  images$: Observable<Image[]>;

  ngOnInit() {
    this.images$ =
    this.imageService.getImages()
    .pipe(
      tap(console.log),
      map(images => images.map(image => {
        const originalImage = image;
        const ref = this.storage.ref(image.original.path);
        originalImage.original.url = ref.getDownloadURL();
        return image;
      }))
    );

    this.images$
    .pipe(
      map(images => images.map(image => {
        const originalImage = image;
        const ref = this.storage.ref(image.original.path);
        originalImage.original.url = ref.getDownloadURL();
        return image;
      }))
    )
    .subscribe(console.log);
  }


  getImage(path: string): Observable<any> {
    const ref = this.storage.ref(path);
    ref.getDownloadURL().pipe(distinctUntilChanged()).subscribe(console.log);
    return ref.getDownloadURL();
  }

  onDetail(id: string) {
    this.router.navigate(['/images', 'show', id]);
  }

  onEdit(id: string) {
    this.router.navigate(['/images', 'edit', id]);
  }

  onDelete(id: string) {
    console.log('delete?');
  }

  ngOnDestroy() {
  }

}
