import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ImageService } from '../image.service';
import { Observable } from 'rxjs';
import { map, switchMap, shareReplay } from 'rxjs/operators';
import { Image, POSITIONS, FITS, GRAVITIES, STRATEGIES } from 'src/app/model/image';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  constructor(
    private storage: AngularFireStorage,
    private route: ActivatedRoute,
    private imageService: ImageService) { }

  id: string;
  images: string[];
  image$: Observable<Image>;
  original$: Observable<any>;
  edited$: Observable<any>;

  Fits = FITS;
  Positions = POSITIONS;
  Gravities = GRAVITIES;
  Strategies = STRATEGIES;

  editForm: FormGroup;

  ngOnInit() {
    this.images = ['Original', 'Edited'];

    this.image$ =
    this.route.paramMap
    .pipe(
      map(param => param.get('id')),
      switchMap(id => this.imageService.getImage(id)),
      shareReplay()
    );

    this.original$ =
    this.image$
    .pipe(
      map(image => image.original.path),
      switchMap(path => this.storage.ref(path).getDownloadURL())
    );

    this.editForm = new FormGroup({
      resize: new FormGroup({
        width: new FormControl(),
        height: new FormControl(),
        fit: new FormControl(),
        position: new FormControl(),
        strategy: new FormControl()
      })
    });

  }

}
