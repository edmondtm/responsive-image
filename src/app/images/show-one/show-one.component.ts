import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Image, ImageSource } from 'src/app/model/image';
import { map, switchMap, tap, shareReplay, take, distinctUntilChanged } from 'rxjs/operators';
import { ImageService } from '../image.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { filter } from 'minimatch';
import { getTreeMissingMatchingNodeDefError } from '@angular/cdk/tree';

@Component({
  selector: 'app-show-one',
  templateUrl: './show-one.component.html',
  styleUrls: ['./show-one.component.css']
})
export class ShowOneComponent implements OnInit, OnDestroy {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private imageService: ImageService,
    private storage: AngularFireStorage) {
      this.id = this.route.snapshot.paramMap.get('id');
    }

  id: string;
  image: Image;
  image$$: Subscription;
  original$: Observable<any>;
  xs$: Observable<any>;
  sm$: Observable<any>;
  md$: Observable<any>;
  lg$: Observable<any>;

  keys: string[];
  selected: string;

  ngOnInit() {
    this.selected = 'original';

    this.image$$ =
    this.route.paramMap
    .pipe(
      map(param => param.get('id')),
      switchMap(id => this.imageService.getImage(id)),
      tap(image => this.keys = Object.keys(image)),
      map(image => {
        this.keys.forEach((obj, index) => {
          image[obj].url = this.storage.ref(image[obj].path).getDownloadURL();
        });
        return image;
      }),
    ).subscribe(image => this.image = image);

  }

  onChange(e) {
    console.log(e);
    this.selected = e.value;
  }

  onEdit() {
    this.router.navigate(['images', 'edit', this.id]);
  }

  onDelete() {
    console.log('onDelete called');
  }

  ngOnDestroy() {
    if (this.image$$) {
      this.image$$.unsubscribe();
    }
  }

}
