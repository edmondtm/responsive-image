import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { random } from 'faker';
import { Observable } from 'rxjs';
import { finalize, take } from 'rxjs/operators';
import { ImageService } from '../image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {


  constructor(
    private storage: AngularFireStorage,
    private imageService: ImageService,
    private router: Router) { }

  file: any;
  percentage: Observable<number>;
  imagePreview: Observable<any>;

  ngOnInit() {
  }

  onUpload(event: any): void {

    this.file = event.target.files[0];

    // Validate file size
    if (this.file.size > 10000000) {
      console.log('too big');
      return;
    }

    // Validate file type
    const type = this.file.type.split('/');
    if (type[0] !== 'image') {
      console.log('not image');
      return;
    }
    if (type[1] !== 'png' && type[1] !== 'jpeg' && type[1] !== 'jpg') {
      console.log('wrong format', type[1]);
      return;
    }

    const path = 'image/' + random.alphaNumeric(8).toString() + '/original/' + this.file.name;

    const task = this.storage.upload(path, this.file);
    const fileRef = this.storage.ref(path);
    this.percentage = task.percentageChanges();

    task.snapshotChanges().pipe(
      finalize(() => {
        this.imagePreview = fileRef.getDownloadURL();
        this.imageService.onUpload({
          original: {
            path,
            type
          }
        }).pipe(
          take(1)
        ).subscribe(
          response => {
            this.router.navigate(['/', 'images', 'show', response.id]);
          },
          error => console.error(error));
      })
    ).subscribe();
  }

}
