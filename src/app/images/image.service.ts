import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Image } from '../model/image';
import { from, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private afs: AngularFirestore) { }

  onUpload(image: Image): Observable<firebase.firestore.DocumentReference> {
    return from(this.afs.collection<Image>('images').add(image));
  }

  getImage(id: string): Observable<Image> {
    return this.afs.doc<Image>('images/' + id)
            .valueChanges();
  }

  getImages(): Observable<Image[]> {
    return this.afs.collection<Image>('images')
            .snapshotChanges()
            .pipe(
              map(images => images.map(image => {
                const id = image.payload.doc.id;
                const data = image.payload.doc.data();
                return Object.assign({
                  id,
                  ...data
                });
              }))
            );
  }
}
