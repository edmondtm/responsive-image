import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImagesRoutingModule } from './images-routing.module';
import { ImagesComponent } from './images.component';
import { ShowAllComponent } from './show-all/show-all.component';
import { ShowOneComponent } from './show-one/show-one.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { MaterialModule } from '../material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [ImagesComponent, ShowAllComponent, ShowOneComponent, CreateComponent, EditComponent],
  imports: [
    CommonModule,
    ImagesRoutingModule,
    MaterialModule,
    FlexLayoutModule
  ]
})
export class ImagesModule { }
