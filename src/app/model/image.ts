import { AngularFireStorageReference } from '@angular/fire/storage';
import { Observable } from 'rxjs';

export interface Image {
    id?: string;

    original: ImageSource;

    xs?: ImageSource;

    sm?: ImageSource;

    md?: ImageSource;

    lg?: ImageSource;

    edited?: ImageSource;
}

export interface ImageSource {
    path: string;
    url?: Observable<any> | string;
    height?: string;
    width?: string;
    type: string;
}

export interface EditPerimeters {
    resize: {
        width: string;
        height: string;
        fit: 'cover' | 'contain' | 'fill' | 'inside' | 'outside';
        position?: 'center' | 'top' | 'right top' | 'right' | 'right bottom' | 'bottom' | 'left bottom' | 'left' | 'left top';
        gravity?: 'north' | 'northeast' | 'east' | 'southeast' | 'south' | 'southwest' | 'west' | 'northwest' | 'center' | 'centre';
        strategy?: 'entropy' | 'attention';
    };
}

export const FITS = ['cover', 'contain', 'fill', 'inside', 'outside'];
export const POSITIONS = ['center', 'top', 'right top', 'right', 'right bottom', 'bottom', 'left bottom', 'left', 'left top'];
export const GRAVITIES = ['north', 'northeast', 'east', 'southeast', 'south', 'southwest', 'west', 'northwest', 'center', 'centre'];
export const STRATEGIES = ['entropy', 'attention'];
