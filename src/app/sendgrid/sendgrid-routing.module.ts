import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SendgridComponent } from './sendgrid/sendgrid.component';

const routes: Routes = [
  {
    path: '',
    component: SendgridComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SendgridRoutingModule { }
