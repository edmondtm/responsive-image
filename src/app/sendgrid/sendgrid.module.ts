import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SendgridRoutingModule } from './sendgrid-routing.module';
import { SendgridComponent } from './sendgrid/sendgrid.component';
import { MaterialModule } from '../material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [SendgridComponent],
  imports: [
    CommonModule,
    SendgridRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class SendgridModule { }
