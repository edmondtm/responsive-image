import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SendgridService {

  constructor(private http: HttpClient) { }

  sendEmail(mail) {
    const path = environment.cloudFunctions + 'sendGrid';
    return this.http.post(path, mail);
  }
}
