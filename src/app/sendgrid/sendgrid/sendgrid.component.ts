import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SendgridService } from '../sendgrid.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-sendgrid',
  templateUrl: './sendgrid.component.html',
  styleUrls: ['./sendgrid.component.css']
})
export class SendgridComponent implements OnInit {

  constructor(private sendgridService: SendgridService) { }

  emailForm: FormGroup;
  submitted: boolean;
  response: Observable<any>;

  ngOnInit() {
    this.submitted = false;
    this.emailForm = new FormGroup({
      api: new FormControl('', Validators.required),
      sender: new FormControl('', [Validators.required, Validators.email]),
      receiver: new FormControl('', [Validators.required, Validators.email]),
      subject: new FormControl('', Validators.required),
      text: new FormControl('', Validators.required),
      html: new FormControl('', Validators.required)
    });
  }

  displayError(field: string, validation: string): boolean {
    const notValid
    = !!this.emailForm.get(field).getError(validation);
    const touched
    = this.emailForm.get(field).touched;
    console.log(`not valid: ${notValid}, touched: ${touched}, submitted: ${this.submitted},
                displayError: ${notValid && (touched || this.submitted)}`);
    return notValid && (touched || this.submitted);
  }

  onSubmit() {
    this.submitted = true;
    if (this.emailForm.valid) {
      this.response = this.sendgridService.sendEmail(this.emailForm.value);
    }
  }

}
