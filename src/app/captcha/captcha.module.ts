import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaptchaRoutingModule } from './captcha-routing.module';
import { CaptchaComponent } from './captcha/captcha.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CaptchaComponent],
  imports: [
    CommonModule,
    CaptchaRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule
  ]
})
export class CaptchaModule { }
