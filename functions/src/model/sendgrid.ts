export interface Sendgrid {
    api: string;
    sender: string;
    receiver: string;
    subject: string;
    text: string;
    html: string;
}
