import * as functions from 'firebase-functions';
import { manupulateImage } from './manupulateImage';
const cors = require('cors')({origin: true});
import * as joi from 'joi';
import { send, setApiKey } from '@sendgrid/mail';
import { Sendgrid } from './model/sendgrid';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.generateThumbnail = functions.storage.object().onFinalize(async (object) => {

    console.log(JSON.stringify(object))

    // Correct Path
    const file: string[] = object.id.split('/');
    const original: boolean = file[1] === 'image' && file[3] === 'original';

    // Correct Image File
    const image = object.contentType === 'image/png' || object.contentType === 'image/jpg'
    || object.contentType === 'image/jpeg';

    // File below 10MB
    const size = +object.size < (10 * 1000 * 1000);

    if (!image) {
        console.error('Wrong Image Type', object.contentType);
        return;
    }

    if (!size) {
        console.error('File Size too big', object.size);
        return;
    }

    // Uploaded Image
    if (original) {
        manupulateImage(object.name, object.bucket);
    }
});

const corsHandler = cors();

exports.sendGrid = functions.https.onRequest(async (req, res) => {
    corsHandler(req, res, async () => {
        const email: Sendgrid = req.body;
        
        const sendgridSchema = joi.object().keys({
            api: joi.string().max(100).required(),
            sender: joi.string().email().max(50).required(),
            receiver: joi.string().email().max(50).required(),
            subject: joi.string().max(100).required(),
            text: joi.string().max(5000).required(),
            html: joi.string().max(10000).required(),
        });

        const msg = {
            to: email.receiver,
            from: email.sender,
            subject: email.subject,
            text: email.text,
            html: email.html,
        };

        try {
            
            const validSchema = joi.validate(email, sendgridSchema);
            if(validSchema.error) {
                throw(validSchema.error)
            }

            setApiKey(email.api);

            send(msg).then(result => {
                return res.json(202).json(result)
            }).catch(
                err => {
                    throw(err);
                }

            )

            return;


        } catch (error) {
            console.error(error);
            return res.status(500).json(error);
        }
        

        


    })
});
