import * as admin from 'firebase-admin';
import * as path from 'path';
import * as os from 'os';
import sharp = require('sharp');
const serviceAccount = require('../resources/sharp-byloka-firebase-adminsdk-s9izk-bf9302f344.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://sharp-byloka.firebaseio.com"
});


export async function manupulateImage(filePath: any, fileBucket: string) {

    const fileArray = filePath.split('/');
    const fileName = fileArray[fileArray.length -1];
    const bucket = admin.storage().bucket(fileBucket);
    const tempFilePath = path.join(os.tmpdir(), fileName);

    // Download Original Image
    await bucket.file(filePath).download({destination: tempFilePath});

    // Resize && optimize
    sharp(tempFilePath)
    .resize(576, 180, {
        fit: 'cover',
        position: sharp.strategy.entropy, 
    })
    .toFormat('webp')


    // Save to storage


    // Delete from local storage

    
    return
};

function convertPath(fileArray: string[], size: string): string {
    // Check if file path points to original
    if (fileArray[2] !== 'original') {
        console.error('File Path is not Original')
    }
    fileArray[2] = size;
    return fileArray.toString().replace(',', '/');
}

// Image Size
// 576 x 180 - xs
// 105 x 180 - sm
// 150 x 180 - md
// 210 x 180 - lg
// 255 x 180 - xl


